from flask_admin.contrib import sqla
from flask import url_for, abort, redirect, request, render_template
from flask_admin import expose, AdminIndexView, BaseView
from flask_login import current_user
import flask_login as login
from wtforms import PasswordField

from routes import *


# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser') or current_user.has_role('Administrator') or current_user.has_role('admin'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        перенаправления пользователей, когда представление недоступно.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))

    # can_edit = True
    edit_modal = True
    create_modal = True
    can_export = True
    can_view_details = True
    details_modal = True


class UserView(MyModelView):
    column_editable_list = ['email', 'name']
    column_searchable_list = column_editable_list
    column_exclude_list = ['password']
    #form_excluded_columns = column_exclude_list
    column_details_exclude_list = column_exclude_list
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }


class CustomView(BaseView):
    @expose('/')
    def index(self):
        return self.render('admin/custom_index.html')
