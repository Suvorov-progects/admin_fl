import datetime

from sqlalchemy_utils import ChoiceType
from wtforms import DateTimeField, StringField, IntegerField

from .connected import Base
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from flask_login import UserMixin, AnonymousUserMixin
from flask_security import RoleMixin
from werkzeug.security import generate_password_hash, check_password_hash

from .connected import sess


# права
class Permission:
    # ПОЛНЫЕ ПРАВА (АДМИНИСТРАТИВНЫЙ ДОСТУП)
    FULL = 0x80
    # ПРОСМАТРИВАТЬ
    VIEWS = 0x01
    # РЕДАКТИРОВАТЬ
    REDACT = 0x02
    # СОЗДАВАТЬ
    CREATE = 0x03
    # НАЗНАЧАТЬ ИСПОЛНИТЕЛЕЙ
    ASSIGN = 0x04
    # ДОБАВЛЯТЬ ДОКУМЕНТЫ
    ADD = 0x05
    # УДАЛЕНИЕ
    DELETED = 0x06


roles_users = Table(
    "user_role", Base.metadata,
    Column("role_id", Integer, ForeignKey("roles.id")),
    Column("user_id", Integer, ForeignKey("users.id"))
)

# если необходимо будет хранить доп данные в промежуточной таблице, то создадим ее как метакласс
# class User_Role(Base):
#     __tablename__ = 'user_role'
#     id = Column(Integer, primary_key=True)
#     role_id = Column(Integer(), ForeignKey("roles.id"))
#     user_id = Column(Integer(), ForeignKey("users.id"))
#     extra_data = Column(String(100))


class Role(Base, RoleMixin):
    __tablename__ = "roles"
    id = Column('id', Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(String(80), unique=True)
    default = Column(Boolean, default=False, index=True)
    permissions = Column(Integer)

    def __str__(self):
        return self.name

    '''
    не создает роль напрямую, а основывается на существующей роли базы данных, 
    а затем обновляет ее. Эта же операция может быть выполнена после изменения роли.
    '''
    @staticmethod
    def insert_roles():
        roles = {
            'Executor': (Permission.VIEWS | Permission.REDACT | Permission.CREATE | Permission.ADD, True),
            'Moderator': (
                Permission.VIEWS | Permission.REDACT | Permission.CREATE | Permission.ADD | Permission.ASSIGN |
                Permission.DELETED, False
            ),
            'Administrator': (0xff, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            sess.add(role)
        sess.commit()


class User(Base, UserMixin):
    BLOCKS = [
        ('QC', u'QC'),
        ('WDI', u'WDI'),
        (u'MTO', u'MTO'),
        (u'LABS', u'LABS'),
        (u'USERS', u'USERS'),
        (u'ITEMS', u'ITEMS'),
        (u'WELDLOG', u'WELDLOG'),
        (u'ISOMETRY', u'ISOMETRY'),
        (u'ANALITICS', u'ANALITICS'),
        (u'FULL_BLOCK', u'FULL_BLOCK')
    ]

    __tablename__ = "users"
    id = Column('id', Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(String(255))
    surname = Column(String(255))
    patronymic = Column(String(255))
    block = Column(ChoiceType(BLOCKS))

    email = Column(String(255), unique=True)
    password = Column(String(255))
    created_on = Column(DateTime)
    updated_on = Column(DateTime)
    # security#########################################################
    # Нужен для security!
    active = Column(Boolean, unique=True, nullable=False)
    fs_uniquifier = Column(String(255), default=False)
    # Для получения доступа к связанным объектам
    role_id = Column(Integer, ForeignKey("roles.id"))
    roles = relationship("Role", secondary=roles_users, backref=backref("users", lazy='dynamic'))
    # Нужен для отслеживания активности
    last_login_at = DateTimeField(null=True)
    current_login_at = DateTimeField(null=True)
    last_login_ip = StringField()
    current_login_ip = StringField()
    login_count = IntegerField(null=True)
    # Нужен для двухфакторный режим авторизации
    tf_totp_secret = StringField()
    tf_primary_method = StringField()
    # Нужен для подтверждение учетной записи
    confirmed_at = DateTimeField(null=True)

    def __init__(self, **kwargs):
        '''Конструктор: сначала вызовите конструктор базового класса,
        если роль не определена после создания объекта базового класса,
        роль определяется в соответствии с доступными блоками'''
        super(User, self).__init__(**kwargs)
        if self.role_id is None:
            if self.block == 'FULL_BLOCK':
                self.role_id = Role.query.filter_by(permissions=0xff).first()
            else:
                self.role_id = 1

    # Flask - Login
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    # Flask-Security
    def has_role(self, *args):
        return set(args).issubset({role.name for role in self.roles})

    # # возвращаем токен пользователя на сессию
    # def get_id(self):
    #     return unicode(self.alternative_id)

    # Требуется для административного интерфейса
    def __unicode__(self):
        return self.username

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def can(self, permissions, block):
        '''Проверьте, разрешена ли роль разрешения, требуемая разрешениями'''
        return self.roles is not None and (self.roles.permissions & permissions) == permissions and self.block == block

    def is_administrator(self):
        '''Проверьте, если администратор'''
        return self.can(Permission.FULL, block='FULL_BLOCK')


class AnonymousUser(AnonymousUserMixin):
    '''Чтобы соответствовать классу User, При входе в систему анонимно
    используйте класс объекта current_user
    '''

    def can(self, permissions):
        return False

    def is_administrator(self):
        return False
