"""3new

Revision ID: 30ccf913897e
Revises: e736dc775e1d
Create Date: 2022-02-03 13:29:24.314690

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = '30ccf913897e'
down_revision = 'e736dc775e1d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('surname', sa.String(length=255), nullable=True))
    op.add_column('users', sa.Column('patronymic', sa.String(length=255), nullable=True))
    op.add_column('users', sa.Column('block', sqlalchemy_utils.types.choice.ChoiceType(), nullable=True))
    op.add_column('users', sa.Column('fs_uniquifier', sa.String(length=255), nullable=True))
    op.alter_column('users', 'active',
               existing_type=sa.BOOLEAN(),
               nullable=False)
    op.drop_constraint('users_username_key', 'users', type_='unique')
    op.create_unique_constraint(None, 'users', ['active'])
    op.drop_column('users', 'username')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('username', sa.VARCHAR(length=255), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'users', type_='unique')
    op.create_unique_constraint('users_username_key', 'users', ['username'])
    op.alter_column('users', 'active',
               existing_type=sa.BOOLEAN(),
               nullable=True)
    op.drop_column('users', 'fs_uniquifier')
    op.drop_column('users', 'block')
    op.drop_column('users', 'patronymic')
    op.drop_column('users', 'surname')
    # ### end Alembic commands ###
