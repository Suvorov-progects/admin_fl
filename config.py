import os

class Config(object):
    DEBUG = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'fsdkfd32r234fsdf'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:postgres@localhost:5432/fadmin'

    ################
    # Flask-Security
    ################

    SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
    SECURITY_PASSWORD_SALT = "fsdfdfsdfdfsdafds"
    # SECURITY_CONFIRMABLE = True

    ################
    # двухфакторный вход в систему

    ################
    # SECURITY_TWO_FACTOR = True
    # все пользователи должны будут настроить и использовать двухфакторную авторизацию
    # SECURITY_TWO_FACTOR_REQUIRED = True
    # Указывает включенные по умолчанию методы для двухфакторной аутентификации
    # SECURITY_TWO_FACTOR_ENABLED_METHODS = 'email'
    # Указывает количество секунд, в течение которых токен доступа действителен.
    # SECURITY_TWO_FACTOR_MAIL_VALIDITY = 86400
    # Указывает адрес электронной почты, на который пользователи отправляют почту,
    # когда им не удается пройти двухфакторную аутентификацию.
    # SECURITY_TWO_FACTOR_RESCUE_MAIL = 'no-reply@localhost'

    ################
    # отслеживание активности пользователя. необходимо в User добавить поля:
    # last_login_at (datetime)
    # current_login_at (datetime)
    # last_login_ip (string)
    # current_login_ip (string)
    # login_count (integer)
    ################
    # SECURITY_TRACKABLE = True
