from functools import wraps
from flask import abort
from flask_login import current_user
from database.models import Permission


def permission_required(permission, block):
    '''Определить декоратор @permission_required (permission)'''
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can(permission, block):
                abort(403)
            '''Если текущий пользователь не имеет разрешения, выдается ошибка 403.'''
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def admin_required(f):
    '''Определить декоратор @admin_required'''
    return permission_required(Permission.FULL, block='FULL_BLOCK')(f)
